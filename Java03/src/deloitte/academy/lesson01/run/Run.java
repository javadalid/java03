package deloitte.academy.lesson01.run;

import java.util.InputMismatchException;
import java.util.Scanner;

import deloitte.academy.lesson01.productos.*;

/**
 * Clase Run (main) permite tener acceso a las funciones de la tienda a trav�s
 * de un men�.
 * 
 * @author Javier Adalid
 *
 */
public class Run {

	public static void main(String[] args) {
		Scanner sn = new Scanner(System.in);
		boolean salir = false;
		int opcion;

		// Se crean productos de prueba
		new Producto("biberon", 100, Categorias.BEBES, 20);
		new Producto("computadora", 12000, Categorias.ELECTRONICA, 200);
		new Producto("paleta", 3.50, Categorias.ALIMENTOS, 1);
		new Producto("traje", 4200, Categorias.ROPA, 10);

		/**
		 * Mientras que la bandera no sea 5 (para salir), contin�a mostrando el men�.
		 */
		while (!salir) {

			System.out.println("Selecciona una opci�n");
			System.out.println("1. A�ade un producto");
			System.out.println("2. Ver los productos existentes de la categor�a");
			System.out.println("3. Vender un producto");
			System.out.println("4. Historial de ventas y stock actual");
			System.out.println("5. Salir");
			/**
			 * Uso del try y catch para poder "atrapar" la excepci�n si no se ingresa un
			 * n�mero.
			 */
			try {
				opcion = sn.nextInt();
				Scanner sc = new Scanner(System.in);
				switch (opcion) {
				case 1:
					System.out.println("Ingresa el nombre del producto: ");
					String nombreP = sc.nextLine();
					System.out.println("Ingresa el precio del producto: ");
					double precio = sc.nextDouble();
					System.out.println("Elige una categor�a: 1." + Categorias.ALIMENTOS + " 2." + Categorias.BEBES
							+ " 3." + Categorias.ELECTRONICA + " 4." + Categorias.ROPA);
					Scanner sc2 = new Scanner(System.in);
					int opcion2 = sn.nextInt();
					Categorias cat = null;
					switch (opcion2) {
					case 1:
						cat = Categorias.ALIMENTOS;
						break;
					case 2:
						cat = Categorias.BEBES;
						break;
					case 3:
						cat = Categorias.ELECTRONICA;
						break;
					case 4:
						cat = Categorias.ROPA;
						break;
					default:
						System.out.println("Ingresa una opcion v�lida");
						;
					}
					System.out.println("A�ade el stock: ");
					int stock = sc.nextInt();
					new Producto(nombreP, precio, cat, stock);
					break;

				case 2:
					System.out.println("Ingresa el nombre de la categor�a");
					String nombre = sc.nextLine();
					Operation.busquedaProd(nombre);
					break;
				case 3:
					System.out.println("Ingresa el nombre del producto a vender");
					nombre = sc.nextLine();
					System.out.println("Ingresa la cantidad");
					int cant = sc.nextInt();
					Operation.vender(nombre, cant);
					break;
				case 4:
					Operation.historial();
					break;
				case 5:
					salir = true;
					System.out.println("�Hasta luego!");
					break;
				default:
					System.out.println("Ingresa una opci�n v�lida");
				}
			} catch (InputMismatchException e) {
				System.out.println("Debes insertar un n�mero");
				sn.next();
			}
		}

	}
}
