package deloitte.academy.lesson01.productos;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase producto. Define cada uno de los productos con sus atributos: nombre,
 * precio, categor�a, stock y vendido.
 * 
 * @author Javier Adalid Schmid
 *
 */
public class Producto {

	private static final Logger LOGGER = Logger.getLogger(Producto.class.getName());

	public static ArrayList<Producto> productos = new ArrayList<Producto>();

	public String nombre;
	public double precio;
	public Categorias categoria;
	public int stock;
	public int vendido = 0;

	/**
	 * Constructor de un producto
	 * 
	 * @param nombre:    nombre del producto, type string.
	 * @param precio:    precio del producto, type double.
	 * @param categoria: categor�a del producto, type Categorias (enum).
	 * @param stock:     cantidad disponible del producto, type int.
	 */
	public Producto(String nombre, double precio, Categorias categoria, int stock) {
		super();
		this.nombre = nombre;
		this.precio = precio;
		this.categoria = categoria;
		this.stock = stock;
		productos.add(this);
	}

	/**
	 * Obtiene la cantidad de vendidos de ese producto
	 * 
	 * @return vendido, type int.
	 */
	public int getVendido() {
		return vendido;
	}

	/**
	 * Permite modificar los vendidos del producto.
	 * 
	 * @param vendido: numero de vendidos del producto, type int.
	 */
	public void setVendido(int vendido) {
		if (getStock() > 0 && vendido <= getStock()) {
			this.vendido = vendido;
			this.setStock(this.getStock() - vendido);
			LOGGER.log(Level.INFO, "Stock actualizado. Quedan: " + getStock());
		} else
			LOGGER.log(Level.SEVERE, "No hay stock suficiente");

	}

	/**
	 * Obtiene el nombre del producto.
	 * 
	 * @return nombre: nombre del producto como string.
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Permite modificar el atributo nombre.
	 * 
	 * @param nombre: nombre del producto type string.
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Devuelve el precio de un producto.
	 * 
	 * @return precio, el precio del producto como double.
	 */
	public double getprecio() {
		return precio;
	}

	/**
	 * Permite modificar el precio del producto.
	 * 
	 * @param precio, el precio como double.
	 */
	public void setprecio(double precio) {
		this.precio = precio;
	}

	/**
	 * Retorna la categor�a del producto
	 * 
	 * @return categoria, type Categoria enum
	 */
	public Categorias getCategoria() {
		return categoria;
	}

	/**
	 * Permite modificar la categor�a del producto.
	 * 
	 * @param categoria: type Categoria.
	 */

	public void setCategoria(Categorias categoria) {
		this.categoria = categoria;
	}

	/**
	 * Devuelve el stock del producto.
	 * 
	 * @return stock, como integer.
	 */

	public int getStock() {
		return stock;
	}

	/**
	 * Permite modificar el stock de un producto
	 * 
	 * @param stock, como integer.
	 */

	public void setStock(int stock) {
		this.stock = stock;
	}

}
