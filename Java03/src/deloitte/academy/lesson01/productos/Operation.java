package deloitte.academy.lesson01.productos;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.productos.Producto;

/**
 * Clase operation en donde se definen los m�todos de: B�squeda de productos,
 * vender producto e historial de ventas y stock.
 * 
 * @author Javier Adalid
 *
 */
public class Operation {

	private static final Logger LOGGER = Logger.getLogger(Operation.class.getName());

	/**
	 * M�todo que se encarga de buscar y mostrar una lista de productos seg�n su
	 * categor�a.
	 * 
	 * @param cat: La categor�a a buscar, type String.
	 */
	public static void busquedaProd(String cat) {
		ArrayList<Producto> prodsCat = new ArrayList<Producto>();
		boolean encontrado = false;
		for (Producto p : Producto.productos) {
			if (p.getCategoria().toString().toLowerCase().equals(cat.toLowerCase())) {
				prodsCat.add(p);
				encontrado = true;
			}
		}
		if (!encontrado)
			LOGGER.log(Level.SEVERE, "No se encontr� la categor�a");
		else {
			System.out.println("Los productos existentes de �sta categor�a son: ");
			for (Producto p : prodsCat) {
				System.out.println(p.getNombre() + " ---- En stock: " + p.getStock());
			}
		}
		System.out.println("----------------------------------------------------------------------------------");
	}

	/**
	 * M�todo que se encarga de vender un producto y registrar en el historial la
	 * venta, resta del stock.
	 * 
	 * @param nombre: nombre del producto, type String
	 * @param cant:   cantidad a vender (resta del stock). Type integer
	 */
	public static void vender(String nombre, int cant) {
		boolean encontrado = false;
		for (Producto p : Producto.productos) {
			if (p.getNombre().toLowerCase().equals(nombre.toLowerCase())) {
				p.setVendido(cant);
				encontrado = true;
			}
		}
		if (!encontrado)
			LOGGER.log(Level.SEVERE, "No se encontr� el producto a vender");

		System.out.println("----------------------------------------------------------------------------------");
	}

	/**
	 * M�todo que imprime el historial de ventas por producto as� como los totales
	 * de ventas y stock. Por el momento, imprime todos los productos existentes.
	 */
	public static void historial() {
		int totalVendidos = 0;
		int totalStock = 0;
		for (Producto p : Producto.productos) {
			totalVendidos += p.getVendido();
			totalStock += p.getStock();
			System.out.println(
					p.getNombre() + " ---- " + p.getVendido() + " vendidos" + " ----  En stock: " + p.getStock());
		}
		System.out.println("El total de productos vendidos es: " + totalVendidos);
		System.out.println("El total de productos en stock es: " + totalStock);
		System.out.println("----------------------------------------------------------------------------------");

	}

}
