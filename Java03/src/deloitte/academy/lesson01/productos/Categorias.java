package deloitte.academy.lesson01.productos;

/**
 * Un enum que contiene las distintas categorķas de productos existentes.
 * 
 * @author Javier Adalid Schmid
 * 
 *
 */
public enum Categorias {
	ELECTRONICA, ROPA, BEBES, ALIMENTOS;
}
